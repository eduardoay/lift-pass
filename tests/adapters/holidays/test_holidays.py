import logging
from datetime import datetime, date

import pytest

import src.adapters.holidays as holidays_adapter

logging.getLogger("lift-pass").setLevel(logging.ERROR)


@pytest.mark.parametrize(
    "lift_date,expected",
    [
        (datetime.strptime("2020-12-25", "%Y-%m-%d").date(), True),
        (datetime.strptime("2020-11-05", "%Y-%m-%d").date(), False)
    ]
)
def test_is_holiday(lift_date: date, expected: bool):
    """
    Should assert if the given date object is registered as a Holiday in database
    :param lift_date: Date to test for holiday
    :param expected: Expected assertion
    :return: Test assertion
    """

    res = holidays_adapter.is_holiday(lift_date)
    assert res == expected
