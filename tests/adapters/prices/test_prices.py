"""Tests for prices adapter"""

import logging

import pytest

import src.adapters.prices as prices_adapter
from src.commons.errors import InvalidPriceTypeError
from src.models import Price

logging.getLogger("lift-pass").setLevel(logging.ERROR)


@pytest.mark.parametrize(
    "lift_type,expected",
    [
        ("Night", Price.from_list([2, "Night", 19])),
        ("Jour", Price.from_list([1, "Jour", 35])),
        ("Random", None)
    ]
)
def test_find_price(lift_type: str, expected: Price):
    """
    Should find a price by his lift type. If there's no Price matching the given type
    it throws an InvalidPriceTypeError Exception
    :param lift_type: Type to be searched in DB
    :param expected: Expected result
    :return: Assertion
    """

    try:
        res = prices_adapter.find_price(lift_type)
        assert res.to_dict() == expected.to_dict()
    except InvalidPriceTypeError:
        if expected is None:
            assert True
    except Exception:
        assert False
