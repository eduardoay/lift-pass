from datetime import datetime, date

import pytest

import src.libs.discounts as discounts
from src.models import Price


@pytest.mark.parametrize(
    "price,is_holiday,age,lift_date,expected",
    [
        (
            Price.from_list([None, "Jour", 35]),
            False,
            20,
            datetime.strptime("2020-05-08", "%Y-%m-%d").date(),
            35
        ),
        (
            Price.from_list([None, "Night", 19]),
            False,
            20,
            datetime.strptime("2020-05-08", "%Y-%m-%d").date(),
            19
        ),
        (
            Price.from_list([None, "Jour", 35]),
            False,
            5,
            datetime.strptime("2020-05-08", "%Y-%m-%d").date(),
            0
        ),
        (
            Price.from_list([None, "Jour", 35]),
            False,
            65,
            datetime.strptime("2020-05-08", "%Y-%m-%d").date(),
            27
        ),
        (
            Price.from_list([None, "Night", 19]),
            False,
            65,
            datetime.strptime("2020-05-08", "%Y-%m-%d").date(),
            8
        ),
        (
            Price.from_list([None, "Jour", 35]),
            False,
            20,
            datetime.strptime("2020-12-25", "%Y-%m-%d").date(),
            35
        ),
        (
            Price.from_list([None, "Jour", 35]),
            False,
            20,
            datetime.strptime("2020-05-04", "%Y-%m-%d").date(),
            23
        )
    ]
)
def test_apply_discount(price: Price, is_holiday: bool, age: int, lift_date: date, expected: float):
    final_price = discounts.apply_discounts(price, is_holiday, age, lift_date)
    assert final_price == expected
