import logging
from datetime import datetime

import pytest

from src.commons.drivers.sqlite import SQLite
from src.repositories import PricesRepository
from src.models import Price

logging.getLogger("lift-pass").setLevel(logging.ERROR)


@pytest.mark.parametrize(
    "lift_type,expected",
    [
        ("Night", Price.from_list((2, "Night", 19))),
        ("Sol", None)
    ]
)
def test_find_one_by_lift_type(lift_type, expected):
    with SQLite() as database:
        price = PricesRepository.find_one_by_lift_type(database, lift_type)

        if price is None and expected is None:
            assert True
            return

        assert price.id == expected.id
