import logging
from datetime import datetime

import pytest

from src.commons.drivers.sqlite import SQLite
from src.repositories import HolidaysRepository
from src.models import Holiday

logging.getLogger("lift-pass").setLevel(logging.ERROR)


@pytest.mark.parametrize(
    "date,expected",
    [
        (
            datetime.strptime("2020-12-25", "%Y-%m-%d").date(),
            Holiday.from_list([1, "Christmas", None])
        ),
        (
            datetime.strptime("2020-12-26", "%Y-%m-%d").date(),
            None
        )
    ]
)
def test_find_by_date(date, expected):
    with SQLite() as database:
        holiday = HolidaysRepository.find_by_date(database, date)

        if holiday is None and expected is None:
            assert True
            return

        assert holiday.name == expected.name
