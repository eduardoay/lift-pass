"""SQLite driver tests"""

import logging

import pytest

from src.commons.drivers.sqlite import SQLite

logging.getLogger("lift-pass").setLevel(logging.ERROR)


def test_connection_as_context():
    with SQLite() as database:
        query = "SELECT 1 + 1"
        res = database.query(query)

        assert res[0][0] == 2


def test_connection_as_object():
    database = SQLite()

    query = "SELECT 1 + 1"
    res = database.query(query)

    database.close()

    assert res[0][0] == 2


def test_query_with_args():
    with SQLite() as database:
        query = "SELECT ? + ?"
        res = database.query(query, 1, 1)

        assert res[0][0] == 2


def test_query_one():
    with SQLite() as database:
        query = "SELECT ? + ?"
        res = database.query_one(query, 1, 1)

        assert res[0] == 2


def test_query_none():
    with SQLite() as database:
        query = "INSERT INTO price (type, cost) VALUES (?, ?)"
        database.query_none(query, "test", 10)

        assert database.last_added_id() == 3
