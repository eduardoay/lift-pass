import logging

import pytest

from src.server import app

logging.getLogger("lift-pass").setLevel(logging.ERROR)


@pytest.mark.parametrize(
    "query,expected",
    [
        ({'age': 20, 'date': '2020-05-08', 'name': 'Jour'}, 35),
        ({'age': 20, 'date': '2020-05-08', 'name': 'Night'}, 19),
        ({'age': 5, 'date': '2020-05-08', 'name': 'Jour'}, 0),
        ({'age': 65, 'date': '2020-05-08', 'name': 'Jour'}, 27),
        ({'age': 65, 'date': '2020-05-08', 'name': 'Night'}, 8),
        ({'age': 20, 'date': '2020-12-25', 'name': 'Jour'}, 35),
        ({'age': 20, 'date': '2020-05-04', 'name': 'Jour'}, 23)
    ]
)
def test_get_prices(query: dict, expected: float):
    with app.test_client() as client:
        response = client.get("/prices", query_string=query, headers={"Authorization": "Bearer api-key"})

    if response.status_code != 200:
        assert False

    json_data = response.get_json()

    assert json_data["cost"] == expected
