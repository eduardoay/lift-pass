"""Main application bootstrap"""

import pathlib

from flask import Flask

import src.routes as routes
from src.swagger import swagger_router

PATH = pathlib.Path(__file__).parent.absolute()

app = Flask(__name__, static_folder=f"{PATH}/static")
app.register_blueprint(swagger_router)
app.register_blueprint(routes.ping_router)
app.register_blueprint(routes.prices_router)
