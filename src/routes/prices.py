"""Prices routes"""

from flask import Blueprint

import src.commons.utils as utils
import src.commons.http as http
import src.handlers.prices as handlers
from src.middlewares.auth import bearer_api_key

router = Blueprint("prices", __name__)


@router.route("/prices", methods=["GET"])
@bearer_api_key()
def get_prices():
    """Obtain the price calculations"""

    try:
        _, query = utils.prepare_request_data()
        query["age"] = int(query["age"])

        return http.json(body=handlers.get_price(**query))
    except Exception as err:
        return http.json_error(err)
