"""Prices handler methods"""

from datetime import datetime

import src.adapters.prices as prices_adapter
import src.adapters.holidays as holidays_adapter
import src.libs.discounts as discounts
from src.commons.logging import logger


def get_price(age: int, name: str, date: str):
    """
    Handle GET /prices method
    :param age: Client age passed throughout queryString
    :param name: Lift name passed throughout queryString
    :param date: Lift date passed throughout queryString
    :return dict: response payload
    """

    logger.fields({
        "client_age": age,
        "lift_type": name,
        "lift_date": date
    }).info("start price calculation")

    date = datetime.strptime(date, "%Y-%m-%d").date()
    price = prices_adapter.find_price(lift_type=name)
    is_holiday = holidays_adapter.is_holiday(date)

    cost = discounts.apply_discounts(price=price, is_holiday=is_holiday, lift_date=date, age=age)

    return {"cost": cost}
