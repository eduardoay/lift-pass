"""Holidays repository actions"""

import datetime

from src.commons.drivers.sqlite import SQLite
from src.models import Holiday

TABLE = "holiday"


class HolidaysRepository:
    """Holidays Repository class"""

    @staticmethod
    def find_by_date(database: SQLite, date: datetime.date):
        """
        Find some holiday by his date
        :param database: Database connection object
        :param date: Date of the holiday
        :return Holiday: Holiday model | None
        """

        date = date.strftime("%Y-%m-%d")

        query = f"SELECT id, name, date FROM {TABLE} WHERE date = ? LIMIT 1"
        res = database.query_one(query, date)

        if not res:
            return None

        return Holiday.from_list(res)
