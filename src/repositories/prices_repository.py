"""Price repository table actions"""

from src.commons.drivers.sqlite import SQLite
from src.models import Price

TABLE = "price"


class PricesRepository:
    """Price repository actions"""

    @staticmethod
    def find_one_by_lift_type(database: SQLite, lift_type: str):
        """
        Find for the first Price record matching the lift_type
        :param database: Database connection object
        :param lift_type: name to be searched
        :return Price: Price object model
        """

        query = f"SELECT id, type, cost FROM {TABLE} WHERE type = ? LIMIT 1"
        res = database.query_one(query, lift_type)

        if not res:
            return None

        return Price.from_list(res)
