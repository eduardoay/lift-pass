"""
Configuration of discount conditions. This file contains the 2 main configurations
about discounts: configs and conditions

Configs
    Contains single operations that have an specific discount by setting definitions of
    field name, operator and expected value

    Available fields
        - age       => int
        - lift_date => date
        - lift_type => str

    Available operators
        General
            - lower
            - lower-equal
            - equal
            - greater-equal
            - greater

        Dates
            - weekday

Conditions
    This are compound rules that can apply compound discounts from the configs list,
    Conditions should be listed with precedence to be applied in case of multiple assertions
    to any condition
"""

import src.libs.discounts.operators as ops

CONFIGS = {
    "age_lower_than_6": {
        "field": "age",
        "operator": ops.LOWER,
        "value": 6,
        "discount": 1
    },
    "age_lower_than_15": {
        "field": "age",
        "operator": ops.LOWER,
        "value": 15,
        "skip": {"monday_discount"},
        "discount": 0.3
    },
    "monday_discount": {
        "field": "date",
        "operator": ops.WKDAY,
        "value": 0,
        "holidays": False,
        "discount": 0.35
    },
    "age_greater_than_64": {
        "field": "age",
        "operator": ops.GREAT,
        "value": 64,
        "discount": 0.25
    },
    "night_age_greater_than_64": {
        "field": "age",
        "operator": ops.GREAT,
        "value": 64,
        "discount": 0.6
    }
}

CONDITIONS = {
    "day": ["age_lower_than_6", "age_lower_than_15", "age_greater_than_64", "monday_discount"],
    "night": ["age_lower_than_6", "night_age_greater_than_64"]
}
