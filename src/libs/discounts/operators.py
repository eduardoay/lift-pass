"""Condition operators"""

# General operators

LOWER = "lower"
LOREQ = "lower-equal"
EQUAL = "equal"
GOREQ = "greater-equal"
GREAT = "greater"

# Date operators

WKDAY = "weekday"
