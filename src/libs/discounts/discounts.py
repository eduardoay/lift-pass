"""Discounts actions"""

import math
from datetime import date

import src.libs.discounts.operators as ops
from src.commons.errors import DiscountFieldError
from src.models import Price
from .conditions import CONDITIONS, CONFIGS


def apply_discounts(price: Price, is_holiday: bool, age: int, lift_date: date):
    """
    Apply configured discounts over a given price
    :param is_holiday: Flag to know if the given date is a registered Holiday
    :param price: Price model to apply the discounts
    :param age: Client age
    :param lift_date: Lift date
    :return float: Final date
    """

    available_discounts = CONDITIONS["day"]

    if price.type == "Night":
        available_discounts = CONDITIONS["night"]

    final_price = price.cost
    skip = set()

    for rule in available_discounts:
        if rule in skip:
            continue

        rule_configs = CONFIGS[rule]

        if "holidays" in rule_configs.keys() and rule_configs["holidays"] is False and is_holiday:
            continue

        final_price, applied = _apply_rule(
            rule=rule_configs, price=final_price, age=age, lift_date=lift_date
        )

        if final_price == 0:
            return 0

        if applied and "skip" in rule_configs.keys():
            skip.update(rule_configs["skip"])

    return math.ceil(final_price)


def _apply_rule(rule: dict, price: float, age: int, lift_date: date):
    """
    Apply a specific discount rule over the current price
    :param rule: Rule to be applied
    :param price: Current price
    :param is_holiday: validate that the lift date is a holiday
    :param age: Client age
    :param lift_date: Lift date
    :return float: Modified price
    """

    if rule["field"] == "age":
        return _apply_age_rule(
            age=age,
            price=price,
            discount=rule["discount"],
            operator=rule["operator"],
            value=rule["value"]
        )

    if rule["field"] == "date":
        return _apply_lift_date_rule(
            lift_date=lift_date,
            price=price,
            discount=rule["discount"],
            operator=rule["operator"],
            value=rule["value"]
        )

    raise DiscountFieldError(root_causes=[rule])


def _apply_age_rule(age: int, discount: float, price: float, operator: str, value: int):
    """
    Apply age condition
    :param age: Current age value
    :param discount: Discount percent
    :param price: Current price
    :param operator: Operation to evaluate rule
    :param value: Expected rule value
    :return float: Updated Price
    """

    if operator == ops.LOWER and age < value:
        price = price - (price * discount)
        return price, True

    if operator == ops.LOREQ and age <= value:
        price = price - (price * discount)
        return price, True

    if operator == ops.EQUAL and age == value:
        price = price - (price * discount)
        return price, True

    if operator == ops.GOREQ and age >= value:
        price = price - (price * discount)
        return price, True

    if operator == ops.GREAT and age > value:
        price = price - (price * discount)
        return price, True

    return price, False


def _apply_lift_date_rule(
    lift_date: date, discount: float, price: float, operator: str, value: int
):
    """
    Apply age condition
    :param lift_date: Current Lift date
    :param discount: Discount percent
    :param price: Current price
    :param operator: Operation to evaluate rule
    :param value: Expected rule value
    :return float: Updated Price
    """

    if operator == ops.WKDAY and lift_date.weekday() == value:
        price = price - (price * discount)
        return price, True

    return price, False
