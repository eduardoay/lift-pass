"""Price model"""

# pylint: disable=C0103


class Price:
    """Price model definition class"""

    def __init__(self):
        self.id = None
        self.type = None
        self.cost = None

    @property
    def id(self):
        """Return id value"""
        return self._id

    @id.setter
    def id(self, value: int):
        """Set id value"""

        if value is not None and not isinstance(value, int):
            raise TypeError("id value should be int")

        self._id = value

    @property
    def type(self):
        """Return name value"""
        return self._type

    @type.setter
    def type(self, value: str):
        """Set name value"""

        if value is not None and not isinstance(value, str):
            raise TypeError("name value should be str")

        self._type = value

    @property
    def cost(self):
        """Return date value"""
        return self._cost

    @cost.setter
    def cost(self, value: int):
        """Set date value"""

        if value is not None and not isinstance(value, int):
            raise TypeError("date value should be int")

        self._cost = value

    def to_dict(self):
        """
        Create a dict with the current model data
        :return dict:
        """

        return {"id": self.id, "name": self.type, "date": self.cost}

    @staticmethod
    def from_list(data: (list, tuple)):
        """
        Create an instance model from a set of data given as list or tuple
        :param data: Data to be loaded in the model
        :return Price: Model instance
        """

        instance = Price()
        instance.id = data[0]
        instance.type = data[1]
        instance.cost = data[2]

        return instance
