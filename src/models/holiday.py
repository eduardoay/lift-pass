"""Price model"""

# pylint: disable=C0103


class Holiday:
    """Price model definition class"""

    def __init__(self):
        self.id = None
        self.name = None
        self.date = None

    @property
    def id(self):
        """Return id value"""
        return self._id

    @id.setter
    def id(self, value: int):
        """Set id value"""

        if value is not None and not isinstance(value, int):
            raise TypeError("id value should be int")

        self._id = value

    @property
    def name(self):
        """Return name value"""
        return self._type

    @name.setter
    def name(self, value: str):
        """Set name value"""

        if value is not None and not isinstance(value, str):
            raise TypeError("name value should be str")

        self._type = value

    @property
    def date(self):
        """Return date value"""
        return self._cost

    @date.setter
    def date(self, value: str):
        """Set date value"""

        if value is not None and not isinstance(value, str):
            raise TypeError("date value should be str and not '%s'" % value.__class__.__name__)

        self._cost = value

    def to_dict(self):
        """
        Create a dict with the current model data
        :return dict:
        """

        return {"id": self.id, "name": self.name, "date": self.date}

    @staticmethod
    def from_list(data: (list, tuple)):
        """
        Create an instance model from a set of data given as list or tuple
        :param data: Data to be loaded in the model
        :return Price: Model instance
        """

        instance = Holiday()
        instance.id = data[0]
        instance.name = data[1]
        instance.date = data[2]

        return instance
