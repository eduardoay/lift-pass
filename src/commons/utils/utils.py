"""Common utilities"""

import uuid

import fastjsonschema
from flask import request

import src.commons.context as context
from src.commons.errors import SchemaError
from src.commons.logging import logger


def store_trace_id(headers):
    """
    Search for TraceId header and store it as global configuration for be used on all logs as part of the context
    :param headers: List of request headers
    """

    trace_id = headers.get("Trace-Id", None)

    if not trace_id:
        trace_id = str(uuid.uuid4())

    logger.context.field("trace_id", trace_id)
    context.set_value("trace_id", trace_id)


def prepare_request_data(schema: dict = None):
    """
    Process al request data, store trace_id in logger context and return request body
    :param schema: JSON schema definition to validate body
    :return: JSON request
    :raise: HandlerError if process fail
    """

    body = request.json
    query = dict(request.args)

    if schema is not None:
        body = validate_json_schema(data=body, schema=schema)

    store_trace_id(request.headers)

    if not body:
        body = {}

    if not query:
        query = {}

    return body, query


def validate_json_schema(data: dict, schema: dict):
    """
    Validate JSON request payload with a given JSON schema
    :param data: JSON request data
    :param schema: JSON schema definition
    """

    try:
        validate = fastjsonschema.compile(schema)
        return validate(data)
    except Exception as err:
        raise SchemaError(err) from err
